%clear all;
close all;
clc;

cla0   = rgb2gray(imread('D:\arbres\base_crop_resize\00000.JPG'));
cla2   = rgb2gray(imread('D:\arbres\base_crop_resize\00200.JPG'));
cla11 = rgb2gray(imread('D:\arbres\base_crop_resize\00900.JPG'));

h1 = extractLBPFeatures(cla0);
h2 = extractLBPFeatures(cla2);
h3 = extractLBPFeatures(cla11);

c02  = (h1-h2).^2;
c011 = (h1-h3).^2;
c211 = (h2-h3).^2;

% figure
% bar([c02; c011; c211]','grouped')
% title('Erreur Quadratique')
% xlabel('LBP Histogram')
% legend('Classe 0 vs Classe 2','Classe 0 vs Classe 11','Classe 2 vs Classe 11')


imageTest = rgb2gray(imread('D:\arbres\base_crop_resize\00002.JPG'));
testlbp =  extractLBPFeatures(imageTest);

t1 = (testlbp - h1).^2;
t2 = (testlbp - h2).^2;
t3 = (testlbp - h3).^2;

figure
bar([t1; t2; t3]','grouped')
title('Erreur Quadratique')
xlabel('LBP Histogram')
legend('test vs Classe 0','test vs Classe 2','test vs Classe 11')