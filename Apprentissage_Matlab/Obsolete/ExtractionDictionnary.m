clc;
clear variables;
close all;

%pkg load image;

path = path();

method = 1; %1:Barycentre 2:On garde tout

nbImage = 1960;
nbClasse = 49;
nbImgClasse = 40;
nbParam=10;

if exist("completeBaseData")
  load BaseData.mat;
endif

%baryCentre dictionnary

clear cible;
cible = cell(10,40,nbClasse);
for i=1:nbClasse-1
  cible(:,:,i) = completeBaseData(:,(1+40*(i-1)):(40*(i)));
  for j=2:nbParam
    dico(i,j) = mean(cell2mat(cible(j,:,i)));
  end
  dico(i,1) = i;
end

%Normaliser les valeurs
for j=2:nbParam
  maxCol(j) = max(dico(:,j));
  minCol(j) = min(dico(:,j));
  
  for i=1:nbClasse-1
    dico(i,j) = (dico(i,j) - minCol(j))/(maxCol(j)-minCol(j));
  end
end
% 

foo = imread(strcat(path,'00025.JPG'));
%imshow(foo);

fooR = foo(:,:,1);
fooG = foo(:,:,2);
fooB = foo(:,:,3);
 
fooHSV = rgb2hsv(foo);
fooTSC = rgb2ntsc(foo);

foo_H = fooHSV(:,:,1);
foo_S = fooHSV(:,:,2);
foo_V = fooHSV(:,:,3);

fooT = fooTSC(:,:,1);
fooS = fooTSC(:,:,2);
fooC = fooTSC(:,:,3);

Rp = mean2(fooR);
Gp = mean2(fooG);
Bp = mean2(fooB);
Hp = mean2(foo_H);
Sp = mean2(foo_S);
Vp = mean2(foo_V);
T = mean2(fooT);
S = mean2(fooS);
C = mean2(fooC);


vect = [66 Rp Gp Bp Hp Sp Vp T S C];
for j=2:nbParam
  vect(j) = (vect(j)-minCol(j))/(maxCol(j)-minCol(j));
  
end
vect

for i=1:nbClasse-1
   dist(i)=0;
   for j=5:8
     dist(i) = dist(i) + sqrt(vect(j)^2 + dico(i,j)^2);
   end
end

[v,count] = min(dist);
count
