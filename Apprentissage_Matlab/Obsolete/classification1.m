clc
clear variables
close all

path = path();

nbClasse = 4;
nbImgClasse = 40;
nbParam=10;

testPath = strcat(path,'test.txt');
file = fopen(testPath,'r');

% Premi�re ligne : nombres d'images
tline = fgetl(file);
nbImage = str2double(tline);
nbImage = 160;

chemins = cell(nbImage, 1);
classes = cell(nbImage, 1);

if (exist('completeBaseData','var')==0)
    i = 1;
    tline = fgetl(file);
    while ischar(tline)
      line = strsplit(tline);
      chemins(i) = line(1);   %r�cup�re le nom des images
      classes(i) = line(2);   %r�cup�re les classes associ�

      tline=fgetl(file);
      i = i+1;
    end
    fclose(file);

    i=1;
    while i<=nbImage

          %Lecture de l'image
           cheminImage = strcat(path,char(chemins(i)));
           im = imread(char(cheminImage));
           imG = imresize(rgb2gray(im),[256 256]);
           completeBaseData(:,i) = extractLBPFeatures(imG,'Upright',false);
           i = i+1;
    end
end

%imtest = imread(char(strcat(path,'00150.JPG')));

%ouverture du fichier train------------------------------------------Train
trainPath = strcat(path,'train.txt');
fichierTest = fopen(trainPath);

% Premi�re ligne : nombres d'images dans le fichier train
tline = fgetl(file);
nbImage = str2double(tline);
nbImage = 160;

%cr�ation des tableaux qui vont contenir les noms de fichiers et de classe
nomImagesTrain = cell(nbImage, 1);
ClasseTest = cell(nbImage, 1);

tline = fgetl(file); %changement de ligne
%r�cup�re tout les noms d'images et de leurs classes
for i=1:nbImage
    line = strsplit(tline); %coupe morceau la ligne
    nomImagesTrain(i) = line(1); %r�cup�re le nom de l'image
    classes(i) = line(2); %r�cup�re la classe de l'image
    
    tline = fgetl(file); %changement de ligne
end

%partie calcul des images de test

dist = zeros(120,4);

for j=1:nbImage
    
    cheminTrain = strcat(path,char(nomImagesTrain(j)));
    imtrain = imread(char(cheminTrain)); %lit les images
    
    %Code de maximillien, calcul distance manhattan
    
    imGtest = imresize(rgb2gray(imtrain),[256 256]);
    %On met le vecteur dans les meme dimensions que les elements du dico
    testFeature = (extractLBPFeatures(imGtest,'Upright',false))'; 

    for i=1:nbImage
        fin1(i)=0;
        fin2(i)=0;
        fin3(i)=0;
        fin4(i)=0;
        for k=1:nbParam 
            fin1(i) = abs(testFeature(k)-completeBaseData(k,i)) + fin1(i);
            fin2(i) = (testFeature(k)-completeBaseData(k,i))^2 + fin2(i);
            fin3(i) = (testFeature(k)-completeBaseData(k,i))^3 + fin3(i);
            fin4(i) = (testFeature(k)-completeBaseData(k,i))^4 + fin4(i);
        end
        fin2(i) = sqrt(fin2(i));
        fin3(i) = fin3(i)^(1/3);
        fin4(i) = fin4(i)^(1/4);
    end

    [v,count] = min(fin1);
    manhattan = round((count-1)/40);

    [v,count] = min(fin2);
    euclidien = round((count-1)/40);

    [v,count] = min(fin3);
    minkowski3 = round((count-1)/40);

    [v,count] = min(fin4);
    minkowski4 = round((count-1)/40);

    dist(j,:) = [manhattan euclidien minkowski3 minkowski4];
end
sol(1:40) = 0;
sol(41:80) = 1;
sol(81:120)=2;
sol(121:160)=3;


man = dist(:,1)-sol';
eucl = dist(:,2)-sol';
mik3 = dist(:,3)-sol';
mik4 = dist(:,4)-sol';

p1 = 100*sum(man(:)==0)/160
p2 = 100*sum(eucl(:)==0)/160
p3 = 100*sum(mik3(:)==0)/160
p4 = 100*sum(mik4(:)==0)/160

