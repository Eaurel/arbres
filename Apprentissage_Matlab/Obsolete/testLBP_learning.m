clc;
%clear variables;
close all;

%TODO method = 1; %1:Barycentre 2:On garde tout
nbImage = 80;
nbClasse = 2;
nbImgClasse = 40;
nbParam=10;

path = path();

%baryCentre dictionnary

clear cible; 

foo = imread(strcat(path,'00005.JPG'));
%imshow(foo);
feature = extractLBPFeatures(rgb2gray(foo),'Upright',false);
feature = feature';

for i=1:nbClasse
   for j=1:nbParam
    
    lp(i,j) = (feature(j)- completeBaseData(j,i))^2;
   end
   guess(i) = sqrt(sum(lp(i,:)));
end

[v,count] = min(guess);
count
