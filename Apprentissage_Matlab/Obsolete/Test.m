clc
%clear variables Commentez pour gagner du temps de build.
close all

%pkg load image; %Necessaire sous Octave

path = path();

testPath = strcat(path,'test.txt');
file = fopen(testPath,'r');

% Premi�re ligne : nombres d'images
tline = fgetl(file);
nbImage = str2double(tline);

checkReload = exist("completeBaseData");

if (checkReload ~= 1 ) %Condition pour eviter le rechargement des donn�es.
  
  % Initialisation des tableaux
  chemins = cell(nbImage, 1);
  classes = cell(nbImage, 1);
  nbClasse = 49;
  % Lecture des chemins et de leurs classes associ�es
  i = 1;
  tline = fgetl(file);
  while ischar(tline)
      line = strsplit(tline);
      chemins(i) = line(1);   %r�cup�re le nom des images
      classes(i) = line(2);   %r�cup�re les classes associ�
      
      tline=fgetl(file);
      i = i+1;
  end

  fclose(file);

  i = 1;

  while i<=80%nbImage
      
      %Lecture de l'image
      im = imread(strcat(path,char(chemins(i))));
      %Transfert dans les autres bases
%       imhsv = rgb2hsv(im);
%       imind = rgb2ind(im);
%       imntsc = rgb2ntsc(im);
%       
%       %Decoupage RGB
%       imR = mean2(im(:,:,1));
%       imG = mean2(im(:,:,2));
%       imB = mean2(im(:,:,3));
%       %Decoupage HSV
%       im_H = mean2(imhsv(:,:,1));
%       im_S = mean2(imhsv(:,:,2));
%       im_V = mean2(imhsv(:,:,3));
%       %Decoupage ntsc 
%       imT = mean2(imntsc(:,:,1));
%       imS = mean2(imntsc(:,:,2));
%       imC = mean2(imntsc(:,:,3));
      
      imG = rgb2gray(im);
      
      %Ajout des propri�t�s a la base d'apprentissage.
      
      %dataVect = [classes(i) imR imG imB im_H im_S im_V imT imS imC];
      completeBaseData(:,i) = extractLBPFeatures(imG,'Upright',false);
      i = i+1;
      
      if(mod(i,100)==0)
          i
      end
  end
end


%%%%%%%%%


