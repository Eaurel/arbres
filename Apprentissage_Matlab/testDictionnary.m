%clear all;
close all;
clc;

timer = tic;

basePath = path();

%Ouvrir le fichier
filePath = strcat(basePath,'test.txt');
file = fopen(filePath,'r');


% Premi�re ligne : nombres d'images
tline = fgetl(file);
nbImage = str2double(tline);
dico = ExtractDictionnary(4,nbImage,false);
%Initialisation des tableaux
  chemins = cell(nbImage, 1);
  classes = cell(nbImage, 1);

  % Lecture des chemins et de leurs classes associ�es
  i = 1;
  tline = fgetl(file);
  while ischar(tline)
      line = strsplit(tline);
      chemins(i) = line(1);   %r�cup�re le nom des images
      classes(i) = line(2);   %r�cup�re les classes associ�

      tline=fgetl(file);
      i = i+1;
  end

  fclose(file);

  %Creation des test par extraction des features de chaques
  %images. 
  i = 1;
  
  while i<=nbImage
     im = imread(char(strcat(path,char(chemins(i)))));
     toTest(i,:) = createFullFeatureVector(im);
     i = i+1; 
  end
  
  [dico,toTest] = extractBestFeature(dico,toTest,500);
  i=1;
  answer1 = [];
  answer2 = [];
  answer3 = [];
  answer4 = [];
  
  while i<=nbImage
      [dim1, dim2, dim3, dim4] = findClosestMatch(toTest(i,:),dico);
      answer1 = [ answer1 dim1];
      answer2 = [ answer2 dim2];
      answer3 = [ answer3 dim3];
      answer4 = [ answer4 dim4];
      i=i+1;
  end



comp1 = sol - answer1;
comp2 = sol - answer2;
comp3 = sol - answer3;
comp4 = sol - answer4;

rate1 = 100*sum(comp1(:)==0)/nbImage
rate2 = 100*sum(comp2(:)==0)/nbImage
rate3 = 100*sum(comp3(:)==0)/nbImage
rate4 = 100*sum(comp4(:)==0)/nbImage
rateFinal = [rate1 rate2 rate3 rate4]
time = toc(timer)