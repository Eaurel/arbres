clc
clear variables
close all

% Chemin de la base et du fichier de test
path = path();
trainPath = strcat(path,'test.txt');

% Chemin pour enregistrer les images modifi�es
pathOutput = strcat(path,'\..\base_crop_resize\');
pathOutput = char(pathOutput);
mkdir(pathOutput);

% Ouverture du fichier train.txt
file = fopen(trainPath,'r');

% Premi�re ligne : nombres d'images
tline = fgetl(file);
nbImage = str2double(tline);

% Initialisation des tableaux
  chemins = cell(nbImage, 1);
  
% Lecture des chemins
i = 1;
tline = fgetl(file);
while ischar(tline)
  line = strsplit(tline);
  chemins(i) = line(1);   %r�cup�re le nom des images
  tline=fgetl(file);
  i = i+1;
end
fclose(file);


% Chrono
tic

j = 1;
while(j <= nbImage)
    image = imread(char(strcat(path,char(chemins(j)))));

    % Calcul de la taille de l'image rogn�e
    [rows, columns, numberOfColorChannels] = size(image);
    
    %Coefficients de rognage � r�gler
    targetWidth = floor(0.6 * rows);
    targetHeight = floor(1 * columns);
    startX = rows / 2 - targetWidth / 2;
    startY = columns / 2 - targetHeight / 2;
    
    %(y , x , h , w) � cause de la rotation de l'image
    rect = [startY startX targetHeight targetWidth];
    
    % Rognage et r�duction
    cropped = imcrop(image,rect);
    outputImage = imresize(cropped,[256, 256]);
    
    outputName = strcat(pathOutput, char(chemins(j)));
    imwrite(outputImage, outputName);
    
    j = j+1;
    
    
end

time = toc
