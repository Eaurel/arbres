function guess = lbpOnly()
    
    tic;
    load("sol.mat");
    
    basePath = path();
    %Ouvrir le fichier
    filePath = strcat(basePath,'train.txt');
    file = fopen(filePath,'r');
    
    lbpNeighbors = 8;
    lbpRotation = true;
    lbpRadius = 1;
    lbpCell = [127 127];

    % Premi�re ligne : nombres d'images
    tline = fgetl(file);
    nbImage = str2double(tline);
    %Initialisation des tableaux
    chemins = cell(nbImage, 1);
    classes = cell(nbImage, 1);

      % Lecture des chemins et de leurs classes associ�es
      i = 1;
      tline = fgetl(file);
      while ischar(tline)
          line = strsplit(tline);
          chemins(i) = line(1);   %r�cup�re le nom des images
          classes(i) = line(2);   %r�cup�re les classes associ�

          tline=fgetl(file);
          i = i+1;
      end

      fclose(file);

      %Creation des test par extraction des features de chaques
      %images. 
      i = 1;

      while i<=nbImage
         im = imread(char(strcat(path,char(chemins(i)))));
         
         vect = createFullFeatureVector(im,lbpRotation,lbpNeighbors,lbpRadius,lbpCell);%[extractLBPFeatures(im(:,:,1)) extractLBPFeatures(im(:,:,2)) extractLBPFeatures(im(:,:,3))];
         testLBP(i,:) = vect;
         
         i = i+1; 
      end
      
    model5 = fitcknn(testLBP,sol, 'NumNeighbors', 1,'Distance','cityblock');
    model6 = fitcknn(testLBP,sol, 'NumNeighbors', 4,'Distance','cityblock');
    
    model1 = fitcknn(testLBP,sol, 'NumNeighbors', 1);
    model2 = fitcknn(testLBP,sol, 'NumNeighbors', 4);
    
    model3 = fitcknn(testLBP,sol, 'NumNeighbors', 1,'Distance','minkowski');
    model4 = fitcknn(testLBP,sol, 'NumNeighbors', 4,'Distance','minkowski');
   
    %-------------------------------------------------------------------------------
    %Phase de test
    %-------------------------------------------------------------------------------
    
    filePath = strcat(basePath,'test.txt');
    file = fopen(filePath,'r');

    % Premi�re ligne : nombres d'images
    tline = fgetl(file);
    %nbImage = str2double(tline);
    %Initialisation des tableaux
    chemins = cell(nbImage, 1);
    classes = cell(nbImage, 1);

  % Lecture des chemins et de leurs classes associ�es
    i = 1;
    tline = fgetl(file);
    while ischar(tline)
        line = strsplit(tline);
        chemins(i) = line(1);   %r�cup�re le nom des images
        classes(i) = line(2);   %r�cup�re les classes associ�

         tline=fgetl(file);
         i = i+1;
    end

    fclose(file);
    %[extractLBPFeatures(rgb2gray(im),'Upright',false)
  i=1;
  while i <= nbImage
     im = imread(char(strcat(path,char(chemins(i)))));
     LBPim = createFullFeatureVector(im,lbpRotation,lbpNeighbors,lbpRadius,lbpCell); %[ extractLBPFeatures(im(:,:,1))  extractLBPFeatures(im(:,:,2)) extractLBPFeatures(im(:,:,3))];
    
     guess1(i)=predict(model1,LBPim);
     guess2(i)=predict(model2,LBPim);
     guess3(i)=predict(model3,LBPim);
     guess4(i)=predict(model4,LBPim);
     guess5(i)=predict(model5,LBPim);
     guess6(i)=predict(model6,LBPim);
     %if(mod(i,300)==0)i
     %end
%      j=1;  
%      while j <= nbImage
%         quadError(j,:) = (LBPim - testLBP(j,:)).^2;
%         errorTab(j) = sum(quadError(j,:));
%         j = j+1; 
%      end
%      
%      [a closestIndex] = min(errorTab);
%      guess(i) = floor((closestIndex-1)/40);
      i = i+1;
  end

    comp1 = guess1-sol;
    comp2 = guess2-sol;
    comp3 = guess3-sol;
    comp4 = guess4-sol;
    comp5 = guess5-sol;
    comp6 = guess6-sol;
    
    rateEucl1 = 100*sum(comp1(:)==0)/nbImage %eucl
    rateEucl4 = 100*sum(comp2(:)==0)/nbImage %eucl
    rateMiko1 = 100*sum(comp3(:)==0)/nbImage%miko
    rateMiko4 = 100*sum(comp4(:)==0)/nbImage%miko
    rateMan1 = 100*sum(comp5(:)==0)/nbImage%man
    rateMan4 = 100*sum(comp6(:)==0)/nbImage%man
  toc
end
