function [guess1, guess2, guess3, guess4] = findClosestMatch( featureVector , dictionnary)
    
    sub = dictionnary - featureVector;
    sizeDico = length(dictionnary(:,1));
    
    i=1;
    guessVect = [];
    while i<=sizeDico(1)
  
        p1 = sum(abs(sub(i,:)));
        p2 = sum(abs(sub(i,:).^2))^(1/2);
        p3 = sum(abs(sub(i,:).^3))^(1/3);
        p4 = sum(abs(sub(i,:).^4))^(1/4);
        i = i+1;
        
        guessVect = [guessVect ; [p1 p2 p3 p4]];
    end
    
    [a lmin1] = min(guessVect(:,1));
    guess1 = floor((lmin1-1)/40);

    [a lmin2] = min(guessVect(:,2));
    guess2 = floor((lmin2-1)/40);

    [a lmin3] = min(guessVect(:,3));
    guess3 = floor((lmin3-1)/40);

    [a lmin4] = min(guessVect(:,4));
    guess4 = floor((lmin4-1)/40);
    
    clear a;
end