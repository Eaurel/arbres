function fullFeatureVector = createFullFeatureVector(im,lbpRotation,lbpNeighbors,lbpRadius,lbpCell)
    
    %RGB
    rgb1 = extractLBPFeatures(im(:,:,1),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    rgb2 = extractLBPFeatures(im(:,:,2),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    rgb3 = extractLBPFeatures(im(:,:,3),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);

    %CMYK
    cmyk = rgb2cmyk(im);
    cmyk1 = extractLBPFeatures(cmyk(:,:,1),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    cmyk2 = extractLBPFeatures(cmyk(:,:,2),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    cmyk3 = extractLBPFeatures(cmyk(:,:,3),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    cmyk4 = extractLBPFeatures(cmyk(:,:,4),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    
    %garbay
    
    %garbay = rgb2garbay(im);
    C1 = 1;%extractLBPFeatures(garbay(:,:,1),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    C2 = 1;%extractLBPFeatures(garbay(:,:,2),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    C3 = 1;%extractLBPFeatures(garbay(:,:,3),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    
    %HSI
    %HSI = rgb2hsi(im);
    hsi1 = 1;%extractLBPFeatures(HSI(:,:,1),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    hsi2 = 1;%extractLBPFeatures(HSI(:,:,2),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    hsi3 = 1;%extractLBPFeatures(HSI(:,:,3),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    
    %HSL
    %HSL = rgb2hsl(im);
    hsl1 = 1;%extractLBPFeatures(HSL(:,:,1),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    hsl2 = 1;%extractLBPFeatures(HSL(:,:,2),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell); 
    hsl3 = 1;%extractLBPFeatures(HSL(:,:,3),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);

    %LCH
    LCH = rgb2lch(im);
    lch1 = extractLBPFeatures(LCH(:,:,1),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    lch2 = extractLBPFeatures(LCH(:,:,2),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    lch3 = extractLBPFeatures(LCH(:,:,3),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    
    %LUV
    LUV= rgb2luv(im);
    luv1 = extractLBPFeatures(LUV(:,:,1),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    luv2 = extractLBPFeatures(LUV(:,:,2),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    luv3 = extractLBPFeatures(LUV(:,:,3),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    
    %upvpl
    UPV = rgb2upvpl(im);
    upv1 = extractLBPFeatures(UPV(:,:,1),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    upv2 = extractLBPFeatures(UPV(:,:,2),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    upv3 = extractLBPFeatures(UPV(:,:,3),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    
    %xyY
    XYY = rgb2xyY(im);
    xyy1 = extractLBPFeatures(XYY(:,:,1),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    xyy2 = extractLBPFeatures(XYY(:,:,2),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    xyy3 = extractLBPFeatures(XYY(:,:,3),'Upright',lbpRotation,'Radius',lbpRadius,'NumNeighbors',lbpNeighbors,'CellSize',lbpCell);
    

    fullFeatureVector = [rgb1 rgb2 rgb3 cmyk1 cmyk2 cmyk3 cmyk4 C1 C2 C3 hsi1 hsi2 hsi3 hsl1 hsl2 hsl3 lch1 lch2 lch3 luv1 luv2 luv3 upv1 upv2 upv3 xyy1 xyy2 xyy3];

    clear XYY;
    clear UPV;
    clear garbay;
    clear cmyk;
    clear HSI;
    clear HSL;
    clear UPV;
end