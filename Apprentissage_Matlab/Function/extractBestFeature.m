function [relevantDico, dataReturn] = extractBestFeature(dico,data,nbWantedFeature)
    %dico = completeBaseData;%[ 1:10 ; 11:20; 21:30;31:40;41:50;51:60;61:70;71:80;81:90;91:100];
    %Parametrage
    nbFeature = length(dico(1,:));

    %Recuperation d'element statistique � propos des param�tres. 
    statsDico = zeros(nbFeature,5);
    for i=1:nbFeature

          statsDico(i,:) = [i, mean(dico(:,i)), (max(dico(:,i)) - min(dico(:,i))), std(dico(:,i)), var(dico(:,i))];
%         statsDico(i,2) = ;
%         statsDico(i,3) = max(dico(:,i)) - min(dico(:,i));
%         statsDico(i,4) = std(dico(:,i));
%         statsDico(i,5) = var(dico(:,i));
    end

    %Tri en fonction de l'ecart type. Plus l'�cart est important, plus les
    %donn�es sont differenciables
    [~, indexOrder] = sort(statsDico(:,4),'descend');
    statsDico = statsDico(indexOrder,:);
    %Creation d'un dictionnaire du nombre de feature voulue.
    for i=1:nbWantedFeature
        featIndex = statsDico(i,1);
        relevantDico(:,i) = dico(:,featIndex); 
        dataReturn(:,i) = data(:,featIndex);
    end
    
    "Best Feature extracted"
end


