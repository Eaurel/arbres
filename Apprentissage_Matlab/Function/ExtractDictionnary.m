function dictionnary = ExtractDictionnary(nbClasse,nbImage,reduce)

    basePath = path();
    
    %Ouvrir le fichier
    filePath = strcat(basePath,'train.txt');
    file = fopen(filePath,'r');

    % Premi�re ligne : nombres d'images
    tline = fgetl(file);
    %nbImage = str2double(tline);
    %nbImage = 160;
    
    %Initialisation des tableaux
      chemins = cell(nbImage, 1);
      classes = cell(nbImage, 1);
      
      % Lecture des chemins et de leurs classes associ�es
      i = 1;
      tline = fgetl(file);
      while ischar(tline)
          line = strsplit(tline);
          chemins(i) = line(1);   %r�cup�re le nom des images
          classes(i) = line(2);   %r�cup�re les classes associ�

          tline=fgetl(file);
          i = i+1;
      end

      fclose(file);
        
      %Creation du Dictionnaire par extraction des features de chaques
      %images. 
      i = 1;
      while i<=nbImage
          
          im = imread(char(strcat(path,char(chemins(i)))));
          dico(i,:) = createFullFeatureVector(im);
          i = i+1;
      end
      
      %Reduction du dictionnaire si l'on veut le reduire au nombre de
      %classe.
      if( reduce == true)
          i=1;
          nbImageParClasse = nbImage/nbClasse;
          while i<=nbImage
              suiv = floor(i/nbImageParClasse);
              prec = floor((i-1)/nbImageParClasse);
              
              if (suiv ~= prec)
                  
              end
              i = i+1;
          end
          strcat('Reduced in ', time, 'seconds')
      end
      
      dictionnary = dico;
      "Extracted dictionnary"
end