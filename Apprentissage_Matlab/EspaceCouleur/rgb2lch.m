function lchImage = rgb2lch(im)

    %déplacement dans l'espace LAB
    LABImage = rgb2lab(im);
    
    %déplacement de l'espace LAB vers LCH
    LCHTransform = makecform('lab2lch');   %déclare la transformation
    lchImage = applycform(LABImage,LCHTransform); %applique la transformation
    
end