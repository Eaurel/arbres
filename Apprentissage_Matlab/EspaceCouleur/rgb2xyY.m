function xyYImage = rgb2xyY(im)

    %passage en xyz
    xyzImage = rgb2xyz(im);
    
    %passage en xyY
    xyYTransform = makecform('xyz2xyl');   %d�clare la transformation
    xyYImage = applycform(xyzImage,xyYTransform); %applique la transformation
    
end