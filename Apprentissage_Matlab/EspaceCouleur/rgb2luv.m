%l'image est � rentrer en param�tre
%renvoi l puis u puis v
function luvImage = rgb2luv(im)

    %d�placement dans l'espace X Y Z
    XyzImage = rgb2xyz(im); %application de la transformation XYZ

    %conversion XYZ � LUV
    LuvTransform = makecform('xyz2uvl'); %d�clare la transformation XYZ
    luvImage = applycform(XyzImage,LuvTransform); %applique la transformation
    
end
