function HSL = rgb2hsl(im)
    %r�cup�re le RGB pour les calculs
    R = im(:,:,1);
    G = im(:,:,2);
    B = im(:,:,3);
    
    %r�cup�re le min et le max des rgb
    maxi = max(max(R,G),B);
    mini = min(min(R,G),B);
    
    %calcul le I
    I = (maxi+mini)./2;
    
    %calcul selon les donn�es du cours
    %calcul de S
    if I > max(I)/2
        S = (maxi-mini)./(2*max(I)-maxi-mini);
    else
        S = (maxi-mini)./ (maxi+mini);
    end
    
    %calcul de H
    if maxi == R
        H = (G-B)./(V-mini);
    elseif maxi == G
        H = 2+((G-B)./(V-mini));
    elseif maxi == B
        H = 4+((R-G)./(V-mini));
    else
        H = zeros(size(I));
    end
    
    %rassemblage en une matrice dimension 3
    HSL = cat(3,H,S,I);
end