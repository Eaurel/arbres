function HSI = rgb2hsi(im)

    %r�cup�re le RGB pour les calculs
    R = im(:,:,1);
    G = im(:,:,2);
    B = im(:,:,3);
    
    %calcul du mod�le HSI
    I = (R+G+B)/3;
    S = 1-((3*min(min(R,G),B)) ./ (R+G+B));
    T = atan( double((sqrt(3)*(G-B))./(2*R-G-B)) );
    
    HSI = cat(3,T,S,I);

end