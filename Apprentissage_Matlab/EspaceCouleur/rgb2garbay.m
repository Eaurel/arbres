function Garbay = rgb2garbay(im)

    %r�cup�re le RGB pour les calculs
    R = double(im(:,:,1));
    G = double(im(:,:,2));
    B = double(im(:,:,3));
    
    %calcul de l'espace de garbay
    A = 1/3 * (log(R)+log(G)+log(B));
    C1 = (sqrt(3)/2)*(log(R)-log(G));
    C2 = log(B)-((log(R)+log(G))./2);
    
    Garbay = cat(3,A,C1,C2);
end