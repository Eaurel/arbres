import java.util.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.*;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class MoveToTrash {

    /*
     * Structure des fichiers - imagePath - classeA - trashName (dossier) - IMG_XXXX
     * - ... - classeB - trashName (dossier) - ... ...
     */

    /* Variables à modifier selon la configuration de l'utilisateur */
    public static final String imagePath = "C:\\Users\\Utilisateur\\Desktop\\ING3\\PIC\\Base\\Base Arbre";
    public static final String trashList = "C:\\Users\\Utilisateur\\Desktop\\ING3\\PIC\\Test\\arbres\\List_Trash\\ListTrash.txt";
    public static final String trashPath = "C:\\Users\\Utilisateur\\Desktop\\ING3\\PIC\\Base\\Trash";

    public static void main(String[] args) {

        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(trashList), "ISO-8859-1"));

            String line = reader.readLine();

            String currentDirectory = null;
            String currentTrashPath = null;

            while (line != null) {

                if (!(currentDirectory == null) && line.startsWith(" + ")) {

                    String image = currentDirectory + "\\" + line.substring(3, line.length());
                    String trash = currentTrashPath + "\\" + line.substring(3, line.length());

                    try {
                        Path temp = Files.move(Paths.get(image), Paths.get(trash));

                        if (temp == null) {
                            System.out.println("Failed to move the file " + image);
                        }
                    } catch (NoSuchFileException e) {
                        System.out.println("No such file " + image);
                    } catch (FileAlreadyExistsException e){

                    }

                } else if (!line.equals("") && !line.equals(" ")) {

                    currentDirectory = imagePath + "\\" + line;
                    currentTrashPath = trashPath + "\\" + line;

                    File directory = new File(currentTrashPath);
                    if (!directory.exists()) {
                        directory.mkdirs();
                    }
                }

                line = reader.readLine();
            }

            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
