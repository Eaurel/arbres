import java.util.*;
import java.io.*;

public class ListTrash {

    /*
     *  Structure des fichiers
     *  - imagePath
     *      - classeA
     *          - trashName (dossier)
     *          - IMG_XXXX
     *          - ...
     *      - classeB
     *          - trashName (dossier)
     *          - ...
     *      ...
     */

    /* Variables à modifier selon la configuration de l'utilisateur */
    public static final String imagePath = "C:\\Users\\Utilisateur\\Desktop\\ING3\\PIC\\Test\\arbres\\Base Arbre";
    public static final String outputPath = "C:\\Users\\Utilisateur\\Desktop\\ING3\\PIC\\Test\\arbres\\List_Trash\\listTrashPart3.txt";
    public static final String trashName = "Trash";

    public static void main(String[] args) {
        Writer writer = null;

        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputPath), "utf-8"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        final File file = new File(imagePath);

        /* Trouver les dossiers dans lesquels sont stockés les images inutilisables */
        for (File classe : file.listFiles()) {
            if (classe.isDirectory()) {
                for (File child : classe.listFiles()) {
                    if (child.getName().equals(trashName)) {

                        /*
                         * Indiquer dans le fichier de sortie que la classe contient des images
                         * inutilisables
                         */
                        try {
                            writer.write(classe.getName() + "\n");
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        /* Écrire le nom de chaque image mise à part */
                        for (File trashed : child.listFiles()) {
                            try {
                                writer.write(" + " + trashed.getName() + "\n");
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                }
            }
        }

        try {
            writer.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
