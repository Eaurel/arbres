import java.io.*;
import java.nio.file.*;

public class BaseCreator {
    public static final int IMAGES_PAR_CLASSE = 80;
    public static final int NB_CLASSES = 50;

    public static void main(String[] args) {

        String imagePath = "D:\\documents\\pic\\bark_vietnam";
        String basePath = "D:\\documents\\pic\\base";
        String baseTrainPath = basePath + "\\train.txt";
        String baseTestPath = basePath + "\\test.txt";
        String namesPath = basePath + "\\names.txt";

        Writer writerTrain = null;
        Writer writerTest = null;
        Writer writerNames = null;

        try {
            writerTrain = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(baseTrainPath), "utf-8"));
            writerTest = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(baseTestPath), "utf-8"));
            writerNames = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(namesPath), "utf-8"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        int imagesParBase = (IMAGES_PAR_CLASSE * NB_CLASSES) / 2;
        try {
            writerTrain.write("" + imagesParBase + "\n");
            writerTest.write("" + imagesParBase + "\n");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        final File file = new File(imagePath);

        int indexClasse = 0;
        int i = 0;

        try {

            for (File classe : file.listFiles()) {
                if (classe.isDirectory()) {
                    File[] images = classe.listFiles();
                    for (int j = 0; (j < IMAGES_PAR_CLASSE); j++) {

                        String imageName = String.format("%05d", i) + ".JPG";
                        String destPath = basePath + "\\" + imageName;

                        try {
                            Files.copy(images[j].toPath(), new File(destPath).toPath(),
                                    StandardCopyOption.REPLACE_EXISTING);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (i % 2 == 0) {
                            writerTrain.write(imageName + " " + indexClasse + "\n");
                        } else {
                            writerTest.write(imageName + " " + indexClasse + "\n");
                        }

                        
                        i++;
                    }
                    System.out.println("Classe " + indexClasse + " " + classe.getName());
                    writerNames.write("Classe " + indexClasse + " " + classe.getName() + "\n");
                    indexClasse++;
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            writerTrain.close();
            writerTest.close();
            writerNames.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}
