function upvplImage = rgb2upvpl(im)
    %passage en xyz
    xyzImage = rgb2xyz(im);
    
    %passage en xyY
    upvplTransform = makecform('xyz2upvpl');   %d�clare la transformation
    upvplImage = applycform(xyzImage,upvplTransform); %applique la transformation
    
end