<<<<<<< HEAD
function Garbay = rgb2garbay(im)

    %r�cup�re le RGB pour les calculs
    R = double(im(:,:,1));
    G = double(im(:,:,2));
    B = double(im(:,:,3));
    
    %calcul de l'espace de garbay
    A = 1/3 * (log(R)+log(G)+log(B));
    A = A * (255/max(max(A)));

    C1 = (sqrt(3)/2)*(log(R)-log(G));
    C1 = (C1 + abs(min(min(C1))));
    C1 = C1 * (255/max(max(C1)));

    C2 = log(B)-((log(R)+log(G))./2);
    C2 = C2 + abs(min(min(C2)));
    C2 = C2 * (255/max(max(C2)));
    
    Garbay = cat(3,A,C1,C2);
=======
function Garbay = rgb2garbay(im)

    %r�cup�re le RGB pour les calculs
    R = double(im(:,:,1));
    G = double(im(:,:,2));
    B = double(im(:,:,3));
    
    %calcul de l'espace de garbay
    A = 1/3 * (log(R)+log(G)+log(B));
    C1 = (sqrt(3)/2)*(log(R)-log(G));
    C2 = log(B)-((log(R)+log(G))./2);
    
    m = cat(3,A,C1,C2);
    min1=min(min(m));
    max1=max(max(m));
    y = uint8(255 .* ((double(m)-double(min1))) ./ double(max1-min1));
    Garbay = y;
>>>>>>> DicoMaking
end