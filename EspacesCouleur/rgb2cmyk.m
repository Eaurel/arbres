function cmykImage= rgb2cmyk(im)
    %d�placement dans l'espace cmyk
    CMYKTransform = makecform('srgb2cmyk');   %d�clare la transformation
    cmykImage = applycform(im,CMYKTransform); %applique la transformation
    
end