import java.util.*;
import java.io.*;

public class Counter
{

  public static final String path = "C:\\Users\\Utilisateur\\Desktop\\ING3\\PIC\\Compteur\\Base Arbre\\";

  public static void main(String[] args)
  {
    Writer writer = null;

    try {
        writer = new BufferedWriter(new OutputStreamWriter(
              new FileOutputStream("nbClasse.txt"), "utf-8"));
    } catch (IOException ex) {
        // Report
    }
    
    final File file = new File(path);
    try{
      writer.write("Il y a : " + file.listFiles().length + " Classes d'arbre \n");
      writer.write(" \n  ");
    }
    catch (Exception ex) {/*ignore*/}

    for(File child : file.listFiles()) {
        try {
          writer.write(child.getName() + " : "+ (child.listFiles().length) + " photos dans la classe.\n");
        }
        catch (Exception ex) {/*ignore*/}
    }

    try {writer.close();} catch (Exception ex) {/*ignore*/}
  }
}
